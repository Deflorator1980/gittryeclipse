package h;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;


public class App {
	static ApplicationContext ac;
    public static void main(String[] args) {
        ArrayList<Iadd> list = new ArrayList<>();
        list.add((setter(Add.class)).getBean(Add.class));
        list.add((setter(Add2.class)).getBean(Add2.class));
        list.add((setter(Add3.class)).getBean(Add3.class));
        list.add((setter(Add4.class)).getBean(Add4.class));
     
        for (Iadd print : list){
            print.show();
        }
    }
    
    public static AnnotationConfigApplicationContext setter(Class<?>... annotatedClasses){
    	
		return new AnnotationConfigApplicationContext(annotatedClasses);
    }
}